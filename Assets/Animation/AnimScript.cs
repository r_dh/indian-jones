﻿using UnityEngine;
using System.Collections;

public class AnimScript : MonoBehaviour 
{
    private Animator _anim;
    private Rigidbody _rb;

    private bool _grappleFinishedPlaying = true;
    private float _grappleAnimCounter = 0.0f;

    private bool _jumpFinishedPlaying = true;
    private float _jumpAnimCounter = 0.0f;

    enum animStates {Idle = 0, Grappling = 1, Falling = 2, Swinging = 3, Jumping = 4, Running = 5}

	// Use this for initialization
	void Start () 
    {
        _anim = GetComponentInChildren<Animator>();
        _rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        //Grapple Animation
        if ((Input.GetButtonDown("Fire (KeyBoard)") || Input.GetButtonDown("Fire (Controller)")))
        {
            _anim.SetInteger("State", (int)animStates.Grappling);
        }
        //Swing Animation
        else if (Player_Grapple._isColliding == true && Grapple._grappledToBox == false)
        {
            _anim.SetInteger("State", (int)animStates.Swinging);
        }
        //Fall Animation
        else if(_rb.velocity.y < -4.0f)
        {
            _anim.SetInteger("State", (int)animStates.Falling);
        }
        //Jump Animation
        else if (_rb.velocity.y > 5.0f)
        {
            _anim.SetInteger("State", (int)animStates.Jumping);
        }
        //Run Animation
        else if(Mathf.Abs(_rb.velocity.x) > 1.0f && (_anim.GetInteger("State") != (int)animStates.Jumping || _anim.GetInteger("State") != (int)animStates.Falling))
        {
            _anim.SetInteger("State", (int)animStates.Running);
        }
        else
        {
           _anim.SetInteger("State", (int)animStates.Idle);
        }       
	}
}
