﻿using UnityEngine;
using System.Collections;

public class Level3_Trigger : MonoBehaviour
{
    public Material ActivatedMaterial = null;
    public GameObject Enemy = null;
    public GameObject BoxCollider = null;
    private GameObject _obj = null;

    private bool _triggered = false;
    private float _delayCounter = 0.0f;
    // Use this for initialization
    void Start()
    {
        _obj = GameObject.Find("GameObject 1");
        Enemy.GetComponent<NavMeshAgent>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(_triggered == true)
        {
            _delayCounter += Time.deltaTime;
        }
        if(_delayCounter >= 3.0f)
        {
            Enemy.GetComponent<NavMeshAgent>().enabled = true;
            _delayCounter = 0.0f;
            _triggered = false;
        }
    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.collider.tag == "Moveable")
        {
            _obj.GetComponent<Rigidbody>().isKinematic = false;
            GetComponent<MeshRenderer>().material = ActivatedMaterial;
            Destroy(BoxCollider);
            Destroy(_obj, 5);
            _triggered = true;
        }
    }
}
