﻿using UnityEngine;
using System.Collections;

public class TargetingScript : MonoBehaviour 
{
    private GameObject _player = null;
    private GameObject _target = null;
    private bool _isSnapped = false;
    public float AimSensitivity = 0.25f;

    //private Material _material = null;

    private float _snapDuration;
	// Use this for initialization
	void Start () 
    {
        _player = GameObject.Find("Player");
        _target = GameObject.Find("Target");
        //_material = GetComponent<MeshRenderer>().material;

	}
	
	// Update is called once per frame
	void Update () 
    {
        //Toggle NoSkill© aimbot visibility
        if(Input.GetButtonDown("Enable Controller") == true)
        {
            if(Player_Control._useController1 == false)
            {
                Player_Control._useController1 = true;
            }
            else
            {
                Player_Control._useController1 = false;
            }
            
        }

        //Only show when controller (AKA peasant stick) is enabled
        if (Player_Control._useController1 == false)
        {
            _target.GetComponent<SpriteRenderer>().enabled = false;
        }
        else
        {
            _target.GetComponent<SpriteRenderer>().enabled = true;
        }

        //Move according to peasant stick secondary joystick
        if(Player_Control._useController1 == true)
        {
            _snapDuration += Time.deltaTime;
            if(_snapDuration >= 1.5f)
            {
                _snapDuration = 0.0f;
                _isSnapped = false;
            }
            if(_isSnapped == false)
            {
                Vector3 move = Vector3.zero;
                move.x = Input.GetAxis("HorizontalAim") * AimSensitivity;
                move.y = Input.GetAxis("VerticalAim") * AimSensitivity;
                this.transform.position += move;
            }     
        }

        //Adjust target position if player moves too far
        if(Mathf.Abs(_player.transform.position.x - _target.transform.position.x) >= 20.0f)
        {
            if(_player.transform.position.x > _target.transform.position.x)
            {
                Vector3 move = Vector3.zero;
                move.x = 1.0f;
                _target.transform.position += move;
            }
            else
            {
                Vector3 move = Vector3.zero;
                move.x = 1.0f;
                _target.transform.position -= move;
            }
        }
        if (Mathf.Abs(_player.transform.position.y - _target.transform.position.y) >= 20.0f)
        {
            if (_player.transform.position.y > _target.transform.position.y)
            {
                Vector3 move = Vector3.zero;
                move.y = 1.0f;
                _target.transform.position += move;
            }
            else
            {
                Vector3 move = Vector3.zero;
                move.y = 1.0f;
                _target.transform.position -= move;
            }
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Target")
        {
            this.gameObject.transform.position = other.transform.position;
            _isSnapped = true;
        }
    }
}
