﻿using UnityEngine;
using System.Collections;

public class MovePlatformScript : MonoBehaviour 
{
	public float Speed;
	public bool Switch;
	public float Distance;
	private float _pos;

	// Use this for initialization
	void Start () 
    {
		Speed = 7.5f;
		Switch = true;
		Distance = 20f;
		_pos = -31.77f;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (transform.position.x > _pos + Distance)
        {
            Switch = false;
        }
        else if (transform.position.x < _pos)
        {
            Switch = true;
        }
        if (Switch == true)
        {
            transform.Translate(new Vector3(1, 0, 0) * Time.deltaTime * Speed);
        }
        else
        {
            transform.Translate(new Vector3(1, 0, 0) * Time.deltaTime * -Speed);
        }	
	}
}
