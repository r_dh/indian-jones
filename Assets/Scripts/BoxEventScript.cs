﻿using UnityEngine;
using System.Collections;

public class BoxEventScript : MonoBehaviour 
{
    private GameObject Gate1 = null;
    private GameObject Gate2 = null;
    private bool _eventTriggered = false;
    private bool _eventDone = false;
    public float EventSpeed = 2.0f;
    private Vector3 _vec1;
    private Vector3 _vec2;
	// Use this for initialization
	void Start () 
    {
        _vec1 = Gate1.transform.position;
        _vec1.y += 10.0f;

        _vec2 = Gate1.transform.position;
        _vec2.y += 10.0f;
	}
	
	// Update is called once per frame
	void Update () 
    {    
	    if(_eventTriggered == true && _eventDone == false)
        {
            float step = EventSpeed * Time.deltaTime;
            Gate1.transform.position = Vector3.MoveTowards(Gate1.transform.position, _vec1, step);
            Gate2.transform.position = Vector3.MoveTowards(Gate2.transform.position, _vec2, step);
        }
        if (Gate1.transform.position == _vec1 && Gate1.transform.position == _vec2)
        {
            _eventDone = true;
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Moveable")
        {
            Vector3 pos = Vector3.zero;
            pos = gameObject.transform.position;
            pos.y = other.transform.position.y;
            other.transform.position = pos;
            other.attachedRigidbody.Sleep();
            _eventTriggered = true;
        }
    }
}
