﻿using UnityEngine;
using System.Collections;

public class ExitScript : MonoBehaviour {
	public Material mat1, mat2;
	public GameObject bordsExit, textExit;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnMouseDown(){
		// this object was clicked - do something
		//Destroy (this.gameObject);
		Debug.Log ("Clicked Exit");
		Application.Quit();
	}

	void OnMouseOver(){
		Debug.Log ("mouse is hovering");
		textExit.GetComponent<MeshRenderer> ().material = mat1;
		//renderer.mat
	}
	
	void OnMouseExit(){
		Debug.Log ("mouse is not hovering :D");
		textExit.GetComponent<MeshRenderer> ().material = mat2;
	}
}
