﻿using UnityEngine;
using System.Collections;

public class RotatePulleyScript : MonoBehaviour {
	public GameObject _rotating1, _rotating2;
	public float _number;
	private Transform _rotate;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		_rotating1.transform.Rotate (new Vector3 (0, 0, 1));
		_rotating2.transform.Rotate (new Vector3 (0, 0, 1));
	
	}
}
