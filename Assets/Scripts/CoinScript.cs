﻿using UnityEngine;
using System.Collections;

public class CoinScript : MonoBehaviour {
    public float RotationSpeed = 120.0f;

    void FixedUpdate() {
        gameObject.transform.Rotate(Time.deltaTime * RotationSpeed, 0, 0);
    }

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            GetComponent<AudioSource>().Play();
            Destroy(gameObject, 0.10f);
        }
    }
}
