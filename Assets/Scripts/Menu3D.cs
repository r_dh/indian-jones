﻿using UnityEngine;
using System.Collections;

public class Menu3D : MonoBehaviour 
{
    public enum States { Main, Pauze, Levels, End };

    private GameObject _menuMain = null;
    private GameObject _mainSub1 = null;
    private GameObject _mainSub2 = null;
    private GameObject _mainSub3 = null;

    private GameObject _menuPauze = null;
    private GameObject _pauzeSub1 = null;
    private GameObject _pauzeSub2 = null;
    private GameObject _pauzeSub3 = null;

    private GameObject _menuEnd = null;
    private GameObject _endSub1 = null;
    private GameObject _endSub2 = null;
    private GameObject _endSub3 = null;

    private GameObject _menuLevels = null;
    private GameObject _levelSub1 = null;
    private GameObject _levelSub2 = null;
    private GameObject _levelSub3 = null;
    private GameObject _levelSub4 = null;

    private GameObject _Selected = null;

    public static bool _menuIsActive = false;
    private int _mainState = 0;
    private float _savedTimeScale = 0.0f;

    public Material matStandard = null;
    public Material matSelected = null;

    private bool _endDelay = false;
    private float _delay = 0.3f;
    private float _delayCounter = 0.0f;

	// Use this for initialization
	void Start () 
    {
	    _menuMain = GameObject.Find("Menu_Main");
        _mainSub1 = _menuMain.transform.Find("Banner1").gameObject;
        _mainSub2 = _menuMain.transform.Find("Banner2").gameObject;
        _mainSub3 = _menuMain.transform.Find("Banner3").gameObject;

        _menuPauze = GameObject.Find("Menu_Pauze");
        _pauzeSub1 = _menuPauze.transform.Find("Banner1").gameObject;
        _pauzeSub2 = _menuPauze.transform.Find("Banner2").gameObject;
        _pauzeSub3 = _menuPauze.transform.Find("Banner3").gameObject;

        _menuEnd = GameObject.Find("Menu_End");
        _endSub1 = _menuEnd.transform.Find("Banner1").gameObject;
        _endSub2 = _menuEnd.transform.Find("Banner2").gameObject;
        _endSub3 = _menuEnd.transform.Find("Banner3").gameObject;

        _menuLevels = GameObject.Find("Menu_Levels");
        _levelSub1 = _menuLevels.transform.Find("Banner1").gameObject;
        _levelSub2 = _menuLevels.transform.Find("Banner2").gameObject;
        _levelSub3 = _menuLevels.transform.Find("Banner3").gameObject;
        _levelSub4 = _menuLevels.transform.Find("Banner4").gameObject;

        if(Application.loadedLevelName == "Level 1")
        {
            _mainState = (int)States.Main;
        }
        else 
        {
            _mainState = (int)States.Pauze;
        }

        _menuMain.SetActive(false);
        _menuPauze.SetActive(false);
        _menuLevels.SetActive(false);
        _menuEnd.SetActive(false);

        ToggleMenu();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if(Input.GetButtonUp("Cancel"))
        {
            ToggleMenu();
        }
        if(_menuIsActive == true)
        {
            ShowMenu(_mainState);
            if(_endDelay == false)
            {
                if (Input.GetAxis("Vertical (KeyBoard)") > 0 || Input.GetAxis("Vertical (Controller)") < 0)
                {
                    DecreaseSubSelect();
                    _endDelay = true;
                }
                if (Input.GetAxis("Vertical (KeyBoard)") < 0 || Input.GetAxis("Vertical (Controller)") > 0)
                {
                    IncreaseSubSelect();
                    _endDelay = true;
                }
                if (Input.GetButtonUp("Jump"))
                {
                    ExecuteSelected(_Selected);
                }
            }
            else
            {
                _delayCounter += Time.deltaTime;
                if(_delayCounter >= _delay)
                {
                    _endDelay = false;
                    _delayCounter = 0.0f;
                }
            }
        }
	}
    public void ToggleMenu()
    {
        if(_menuIsActive == false)
        {
            _menuIsActive = true;
            if (_mainState == (int)States.Main)
            {
                _Selected = _mainSub1;
            }
            if (_mainState == (int)States.Levels)
            {
                _Selected = _levelSub1;
            }
            if (_mainState == (int)States.Pauze)
            {
                _Selected = _pauzeSub1;
            }
            if (_mainState == (int)States.End)
            {
                _Selected = _endSub1;
            }
            Select(_Selected);
            //PauseGame();
        }
        else
        {
            _menuIsActive = false;
            _menuMain.SetActive(false);
            _menuPauze.SetActive(false);
            _menuLevels.SetActive(false);
            _menuEnd.SetActive(false);
            //UnpauseGame();
            Unselect(_Selected);
        }


    }
    void ShowMenu(int mainState)
    {
        if(mainState == (int)States.Main)
        {
            _menuMain.SetActive(true);
            _menuPauze.SetActive(false);
            _menuLevels.SetActive(false);
            _menuEnd.SetActive(false);
        }
        if (mainState == (int)States.Levels)
        {
            _menuLevels.SetActive(true);
            _menuMain.SetActive(false);
            _menuPauze.SetActive(false);
            _menuEnd.SetActive(false);
        }
        if (mainState == (int)States.Pauze)
        {
            _menuMain.SetActive(false);
            _menuPauze.SetActive(true);
            _menuLevels.SetActive(false);
            _menuEnd.SetActive(false);
        }
        if (mainState == (int)States.End)
        {
            _menuMain.SetActive(false);
            _menuPauze.SetActive(false);
            _menuLevels.SetActive(false);
            _menuEnd.SetActive(true);
        }
    }
    void Select(GameObject obj)
    {
        obj.GetComponent<MeshRenderer>().material = matSelected;
    }
    void Unselect(GameObject obj)
    {
        obj.GetComponent<MeshRenderer>().material = matStandard;
    }
    void IncreaseSubSelect()
    {
        Unselect(_Selected);
        if (_mainState == (int)States.Main)
        {
            if (_Selected == _mainSub1)
            {
                _Selected = _mainSub2;
                Select(_Selected);
                return;
            }
            if (_Selected == _mainSub2)
            {
                _Selected = _mainSub3;
                Select(_Selected);
                return;
            }
            if (_Selected == _mainSub3)
            {
                _Selected = _mainSub1;
                Select(_Selected);
                return;
            }
        }
        if (_mainState == (int)States.Pauze)
        {
            if (_Selected == _pauzeSub1)
            {
                _Selected = _pauzeSub2;
                Select(_Selected);
                return;
            }
            if (_Selected == _pauzeSub2)
            {
                _Selected = _pauzeSub3;
                Select(_Selected);
                return;
            }
            if (_Selected == _pauzeSub3)
            {
                _Selected = _pauzeSub1;
                Select(_Selected);
                return;
            }
        }
        if (_mainState == (int)States.Levels)
        {
            if (_Selected == _levelSub1)
            {
                _Selected = _levelSub2;
                Select(_Selected);
                return;
            }
            if (_Selected == _levelSub2)
            {
                _Selected = _levelSub3;
                Select(_Selected);
                return;
            }
            if (_Selected == _levelSub3)
            {
                _Selected = _levelSub4;
                Select(_Selected);
                return;
            }
            if (_Selected == _levelSub4)
            {
                _Selected = _levelSub1;
                Select(_Selected);
                return;
            }
        }
        if (_mainState == (int)States.End)
        {
            if (_Selected == _endSub1)
            {
                _Selected = _endSub2;
                Select(_Selected);
                return;
            }
            if (_Selected == _endSub2)
            {
                _Selected = _endSub3;
                Select(_Selected);
                return;
            }
            if (_Selected == _endSub3)
            {
                _Selected = _endSub1;
                Select(_Selected);
                return;
            }
        }
    }
    void DecreaseSubSelect()
    {
        Unselect(_Selected);
        if (_mainState == (int)States.Main)
        {
            if (_Selected == _mainSub1)
            {
                _Selected = _mainSub3;
                Select(_Selected);
                return;
            }
            if (_Selected == _mainSub2)
            {
                _Selected = _mainSub1;
                Select(_Selected);
                return;
            }
            if (_Selected == _mainSub3)
            {
                _Selected = _mainSub2;
                Select(_Selected);
                return;
            }
        }
        if (_mainState == (int)States.Pauze)
        {
            if (_Selected == _pauzeSub1)
            {
                _Selected = _pauzeSub3;
                Select(_Selected);
                return;
            }
            if (_Selected == _pauzeSub2)
            {
                _Selected = _pauzeSub1;
                Select(_Selected);
                return;
            }
            if (_Selected == _pauzeSub3)
            {
                _Selected = _pauzeSub2;
                Select(_Selected);
                return;
            }
        }
        if (_mainState == (int)States.Levels)
        {
            if (_Selected == _levelSub1)
            {
                _Selected = _levelSub4;
                Select(_Selected);
                return;
            }
            if (_Selected == _levelSub2)
            {
                _Selected = _levelSub1;
                Select(_Selected);
                return;
            }
            if (_Selected == _levelSub3)
            {
                _Selected = _levelSub2;
                Select(_Selected);
                return;
            }
            if (_Selected == _levelSub4)
            {
                _Selected = _levelSub3;
                Select(_Selected);
                return;
            }
        }
        if (_mainState == (int)States.End)
        {
            if (_Selected == _endSub1)
            {
                _Selected = _endSub3;
                Select(_Selected);
                return;
            }
            if (_Selected == _endSub2)
            {
                _Selected = _endSub1;
                Select(_Selected);
                return;
            }
            if (_Selected == _endSub3)
            {
                _Selected = _endSub2;
                Select(_Selected);
                return;
            }
        }
    }
    void ExecuteSelected(GameObject selected)
    {
        if(_mainState == (int)States.Main)
        {
            if(selected == _mainSub1)
            {
                Application.LoadLevel(0);
                return;
            }
            if (selected == _mainSub2)
            {
                _mainState = (int)States.Levels;
                _Selected = _levelSub1;
                return;
            }
            if (selected == _mainSub3)
            {
                Application.Quit();
                return;
            }
        }
        if (_mainState == (int)States.Pauze)
        {
            if (selected == _pauzeSub1)
            {
                ToggleMenu();
                return;
            }
            if (selected == _pauzeSub2)
            {
                Application.LoadLevel(Application.loadedLevel);
                ToggleMenu();
                return;
            }
            if (selected == _pauzeSub3)
            {
                Application.LoadLevel(0);
                SetMainState((int)States.Main);
                return;
            }
        }
        if (_mainState == (int)States.Levels)
        {
            if (selected == _levelSub1)
            {
                Application.LoadLevel(0);
                return;
            }
            if (selected == _levelSub2)
            {
                Application.LoadLevel(1);
                return;
            }
            if (selected == _levelSub3)
            {
                Application.LoadLevel(2);
                return;
            }
            if (selected == _levelSub4)
            {
                _mainState = (int)States.Main;
                _Selected = _mainSub1;
                return;
            }
        }
        if (_mainState == (int)States.End)
        {
            if (selected == _endSub1)
            {
                Application.LoadLevel(Application.loadedLevel + 1);
                return;
            }
            if (selected == _endSub2)
            {
                Application.LoadLevel(Application.loadedLevel);
                ToggleMenu();
                return;
            }
            if (selected == _endSub3)
            {
                Application.LoadLevel(0);
                SetMainState((int)States.Main);
                return;
            }
        }
    }
    public void SetMainState(int state)
    {
        _mainState = state;
    }
}
