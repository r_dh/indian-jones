﻿using UnityEngine;
using System.Collections;

public class CheckpointScript : MonoBehaviour {
    public Light _light;

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Player" && Mathf.Abs(_light.intensity - 1.5f) > 0.0001f) {
            RespawnScript.RespawnPos = new Vector3(transform.position.x, transform.position.y, 0);
            Debug.Log("Player Respawn set to " + RespawnScript.RespawnPos.ToString());
            _light.intensity = 1.5f;
        }
    }
}
