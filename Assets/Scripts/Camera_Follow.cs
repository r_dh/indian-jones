﻿using UnityEngine;
using System.Collections;

public class Camera_Follow : MonoBehaviour {
   public float Distance;

    void Update() {
        if (FreeCameraScripts.IsFreeCameraActive() == false) {
            this.transform.position += (GameObject.Find("Player").transform.position + new Vector3(0.6f, 3.2f, -Distance) - transform.position) / 20f;
        }
    }
}
