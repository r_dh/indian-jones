﻿using UnityEngine;
using System.Collections;

public class FreeCameraScripts : MonoBehaviour 
{
    private Vector3 _origPos = Vector3.zero;
    private Quaternion _origRot;
    public static bool _isActivated = false;
    private Camera _mainCamera = null;

    private bool _cameraBoost = false;

    public float CameraSpeed = 0.5f;
    public float AccelerationBoost = 1.5f;

    private Vector2 _cameraSmooth;
    private Vector2 _cameraAbsolute;

    private Vector2 _clampInDegrees = new Vector2(360.0f, 360.0f);
    public Vector2 Sensitivity = new Vector2(0.25f, 0.25f);
    public Vector2 Smoothing = new Vector2(3.0f, 3.0f);
    private Vector2 _targetDirection;
	void Start () 
    {
        _mainCamera = Camera.main;

        _targetDirection = transform.localRotation.eulerAngles;
	}
	
	void Update () 
    {
	    if(Input.GetButtonUp("FreeCamera"))
        {
            if(_isActivated == true)
            {
                _mainCamera.transform.position = _origPos;
                _mainCamera.transform.rotation = _origRot;
                _isActivated = false;
            }
            else
            {
                _isActivated = true;
            }
        }

        if(_isActivated == true)
        {
            var targetOrientation = Quaternion.Euler(_targetDirection);

            var camDelta = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

            camDelta = new Vector2(Input.GetAxisRaw("Controller X"), -Input.GetAxisRaw("Controller Y"));

            camDelta = Vector2.Scale(camDelta, new Vector2(Sensitivity.x * Smoothing.x, Sensitivity.y * Smoothing.y));

            _cameraSmooth.x = Mathf.Lerp(_cameraSmooth.x, camDelta.x, 1.0f / Smoothing.x);
            _cameraSmooth.y = Mathf.Lerp(_cameraSmooth.y, camDelta.y, 1.0f / Smoothing.y);

            _cameraAbsolute += _cameraSmooth;

            if (_clampInDegrees.x < 360.0f)
            {
                _cameraAbsolute.x = Mathf.Clamp(_cameraAbsolute.x, -_clampInDegrees.x * 0.5f, _clampInDegrees.x * 0.5f);
            }

            var xRotation = Quaternion.AngleAxis(-_cameraAbsolute.y, targetOrientation * Vector3.right);
            transform.localRotation = xRotation;

            if (_clampInDegrees.y < 180.0f)
            {
                _cameraAbsolute.y = Mathf.Clamp(_cameraAbsolute.y, -_clampInDegrees.y * 0.5f, _clampInDegrees.y * 0.5f);
            }

            transform.localRotation *= targetOrientation;
            var yRotation = Quaternion.AngleAxis(_cameraAbsolute.x, transform.InverseTransformDirection(Vector3.up));
            transform.localRotation *= yRotation;

            if (Input.GetButtonDown("CameraBoost"))
            {
                _cameraBoost = true;
            }
            if (Input.GetButtonUp("CameraBoost"))
            {
                _cameraBoost = false;
            }

            if (Input.GetAxis("Horizontal (KeyBoard)") != 0)
            {
                transform.Translate(Camera.main.transform.right * CameraSpeed * Input.GetAxis("Horizontal (KeyBoard)"));
            }
            if (Input.GetAxis("Vertical (KeyBoard)") != 0)
            {
                transform.Translate(Camera.main.transform.forward * CameraSpeed * Input.GetAxis("Vertical (KeyBoard)"));
            }
            if (Input.GetAxis("Horizontal (Controller)") != 0)
            {
                transform.Translate(Camera.main.transform.right * CameraSpeed/2.0f * Input.GetAxisRaw("Horizontal (Controller)"));
            }
            if (Input.GetAxis("Vertical (Controller)") != 0)
            {
                transform.Translate(-Camera.main.transform.forward * CameraSpeed/2.0f * Input.GetAxisRaw("Vertical (Controller)"));
            }
        }      
	}

    public static bool IsFreeCameraActive()
    {
        return _isActivated;
    }
}
