﻿using UnityEngine;
using System.Collections;

public class LookAtMouse : MonoBehaviour {

    // speed is the rate at which the object will rotate
    public float speed;
    public Quaternion rotation;

    void FixedUpdate() {
        // Generate a plane that intersects the transform's position with an upwards normal.
        Plane playerPlane = new Plane(Vector3.up, transform.position);

        // Generate a ray from the cursor position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        float hitdist = 0.0f;
        // If the ray is parallel to the plane, Raycast will return false.
        if (playerPlane.Raycast(ray, out hitdist)) {
            // Get point
            Vector3 targetPoint = ray.GetPoint(hitdist);

            // Determine the target rotation
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);

            rotation = Quaternion.Slerp(transform.rotation, targetRotation, speed * Time.deltaTime);
            transform.rotation = rotation;

        }
    }
}