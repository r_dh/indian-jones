﻿using UnityEngine;
using System.Collections;

public class LooseObject_Grapple : MonoBehaviour {

    public Vector3 _grappleTarget;
    public GameObject GrappleHook;
    public float GrappleSpeed = 50f;

    private GameObject _grapple;
    private GameObject _player;
    private Vector3 _playerPos;
    private Ray _hitPos;
    private SpringJoint _springJoint;

    private bool updatePos = false;
    void Start() {
        _springJoint = GetComponent<SpringJoint>();
       _player =  GameObject.FindGameObjectWithTag("Player");
    }

    void Update() {
        if (!_grapple) {
            _springJoint.autoConfigureConnectedAnchor = true;
            _springJoint.connectedAnchor = gameObject.GetComponent<Transform>().position;
            _springJoint.minDistance = 0.1f;
            _springJoint.maxDistance = 5.0f;
            return;
        }
        _springJoint.connectedAnchor = gameObject.GetComponent<Transform>().position;

        if (updatePos) {
            if (_grapple.GetComponent<Grapple>().IsLooseObject()) {
                _playerPos = _player.transform.position; 
                _springJoint.connectedAnchor = _player.transform.position;
            }

            if (Input.GetButton("IncreaseRope") || Input.GetAxisRaw("IncreaseRope") == 1) {
                _springJoint.maxDistance += Time.deltaTime * 10;
                _springJoint.maxDistance = Mathf.Clamp(_springJoint.maxDistance, 2, 20);
            }

            if (Input.GetButton("DecreaseRope") || Input.GetAxisRaw("DecreaseRope") == 1) {
                _springJoint.maxDistance -= Time.deltaTime * 10;
               _springJoint.maxDistance = Mathf.Clamp(_springJoint.maxDistance, 2, 20);
            }

            if (Input.GetButton("Retract")) {
                updatePos = false;
            }

            _grapple.GetComponent<Transform>().position = (Vector3.MoveTowards(_grapple.GetComponent<Transform>().position, gameObject.GetComponent<Transform>().position, GrappleSpeed * Time.deltaTime));
        }
        if (_springJoint.maxDistance < Vector3.Distance(_playerPos, transform.position)) {
            GetComponent<Rigidbody>().AddForce(_playerPos.x - transform.position.x, 0, 0);
        }
    }

    void OnTriggerEnter(Collider c) {
        if (c.tag == "Grapple") {
            _grapple = c.gameObject;
            if (_springJoint) {
                _springJoint.autoConfigureConnectedAnchor = false;
                _springJoint.maxDistance = 0.5f * Vector3.Distance(_playerPos, transform.position); //(1 - _springJoint.damper)
                _springJoint.minDistance = Mathf.Min(_springJoint.maxDistance / 2.0f, 2.0f);
            }
            updatePos = true;
        }
    }

    void OnTriggerExit(Collider cInfo) {
        if (cInfo.tag == "Grapple") {
            _grapple = null;
            if (_springJoint) _springJoint.autoConfigureConnectedAnchor = true;
        } else Debug.Log(cInfo.tag);
    }

}
