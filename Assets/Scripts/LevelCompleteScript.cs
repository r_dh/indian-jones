﻿using UnityEngine;
using System.Collections;

public class LevelCompleteScript : MonoBehaviour {
	public GameObject lever, text;
	public float timer = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay(Collider other)
	{
		if (other.tag == "Player") {
			text.GetComponent<MeshRenderer>().enabled = true;
			if (Input.GetKey ("e")){
				Debug.Log ("The up arrow is pressed");
				timer++;
				if (timer <= 100 ){
				lever.transform.Rotate (new Vector3(0,0,-.5f));
				}
			}	
			if (timer == 100) {
				Debug.Log ("level is complete");
				Invoke( "ChangeLevel", 2.0f );
			}
		}
	}

	void ChangeLevel(){
		Debug.Log ("opening level 2");
		Application.LoadLevel ("Level2");
	}
}
