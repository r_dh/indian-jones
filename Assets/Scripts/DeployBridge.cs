﻿using UnityEngine;
using System.Collections;

public class DeployBridge : MonoBehaviour {
	public Rigidbody Planks;

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Player")
		{
			Debug.Log("Deploy Bridge!");
            Planks.GetComponent<Crumble>().StartCrumbling();
			GetComponent<AudioSource>().Play();
			Destroy(gameObject, 1.5f);
			Destroy(Planks, 5f);
		}
	}
}
