﻿using UnityEngine;
using System.Collections;

public class EndLevelFireTrigger : MonoBehaviour {
	public Light _light, _light2;
	public ParticleSystem _fire, _fire2, _fire3;


	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Player")
		{
			Debug.Log("Activate Fire");
			_light.intensity = 5f;
			_light2.intensity = 5f;
			_fire.Emit(10);
			_fire2.Emit(10);
			_fire3.Emit (10);

            Camera.main.GetComponent<Menu3D>().SetMainState((int)Menu3D.States.End);
            Camera.main.GetComponent<Menu3D>().ToggleMenu();


			GetComponent<AudioSource>().Play ();
		}
	}
}
