﻿using UnityEngine;
using System.Collections;

public class KillScript : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.transform.position = RespawnScript.RespawnPos;
            GameObject.Find("Player").GetComponent<Player_Grapple>().Die();

        }
    }
}
