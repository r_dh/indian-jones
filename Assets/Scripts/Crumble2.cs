﻿using UnityEngine;
using System.Collections;

public class Crumble2 : MonoBehaviour
{
    private float _gravity = 39.81f;
    private float _verticalVelocity = 0.0f;
    private bool drop = false;

    // Update is called once per frame
    void Update()
    {
        if (drop)
        {
            _verticalVelocity -= _gravity * Time.deltaTime;
            float disp = _verticalVelocity * Time.deltaTime;
            gameObject.transform.Translate(new Vector3(disp, 0, 0));
        }
    }

    void Drop()
    {
        drop = true;
    }

    public void StartCrumbling()
    { //call this method
        GameObject.Destroy(gameObject, 5.0f);
        Invoke("Drop", Random.Range(0.0f, 3.0f));
    }
}
