﻿using UnityEngine;
using System.Collections;

public class Level3_Trigger3 : MonoBehaviour
{
    private GameObject Gate2 = null;
    private bool _eventTriggered = false;
    private bool _eventDone = false;
    public float EventSpeed = 2.0f;
    private Vector3 _vec2;

    public Material ActivatedMaterial = null;
    // Use this for initialization
    void Start()
    {
        Gate2 = GameObject.Find("Gate 2");

        _vec2 = Gate2.transform.position;
        _vec2.y += 10.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (_eventTriggered == true && _eventDone == false)
        {
            float step = EventSpeed * Time.deltaTime;
            Gate2.transform.position = Vector3.MoveTowards(Gate2.transform.position, _vec2, step);
        }
        if (Gate2.transform.position == _vec2)
        {
            _eventDone = true;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Moveable")
        {
            Vector3 pos = Vector3.zero;
            pos = gameObject.transform.position;
            pos.y = other.transform.position.y;
            other.transform.position = pos;
            other.attachedRigidbody.Sleep();
            _eventTriggered = true;

            GetComponent<MeshRenderer>().material = ActivatedMaterial;
        }
    }
}
