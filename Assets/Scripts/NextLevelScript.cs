﻿using UnityEngine;
using System.Collections;

public class NextLevelScript : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Debug.Log("Next Level!");
            int index = Application.loadedLevel;
            Application.LoadLevel(index + 1);      
        }
    }
}
