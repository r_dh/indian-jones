﻿using UnityEngine;
using System.Collections;

public class SlideOff : MonoBehaviour {
    public bool isUphill = true;
    public float SlideForce = 1000;
    void OnTriggerStay(Collider other) {
        if (other.tag == "Player") {
            GameObject grapple = GameObject.FindWithTag("Grapple");
            //Rigidbody wakey wakey
            Vector3 pos = other.transform.position;
            other.transform.position = pos;
            //Player looses control
            other.GetComponent<Player_Control>().Speed = 0;
            //hanging onto a rope
            if (grapple && grapple.GetComponent<Grapple>().IsColliding()) return;
            //add speed
            other.GetComponent<Rigidbody>().AddForce(isUphill ? -SlideForce : SlideForce, 0, 0);
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag == "Player") other.GetComponent<Player_Control>().Speed = 15;
    }
}
