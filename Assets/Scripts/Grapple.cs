﻿using UnityEngine;
using System.Collections;

public class Grapple : MonoBehaviour {

    public bool _collision = false;
    private bool _looseObject = false;
    public static bool _grappledToBox = false;
    public bool IsColliding() {
        return _collision;
    }
    public bool IsLooseObject() {
        return _looseObject;
    }
    void OnTriggerEnter(Collider c) {
        if (c.tag == "Moveable") {
            _looseObject = true;
            _grappledToBox = true;
        }
        else
        {
            _grappledToBox = false;
        }

		if (c.tag != "Player" && c.tag != "Coins" && c.name != "BlockingVolume" && c.tag != "Enemy" && c.tag != "Ladder" && c.tag != "Sliding") {
            _collision = true;
            transform.parent = c.transform;
        }

        if (c.tag == "Enemy") {
            GameObject.Find("Player").GetComponent<Player_Grapple>().ForceDisconnect();
            Destroy(c.gameObject);
        }
    }

    void OnTriggerExit(Collider cInfo) {
        if (cInfo.tag != "Player" && cInfo.tag != "Enemy") {
            _collision = false;
            _looseObject = false;
            _grappledToBox = false;
            transform.parent = null;
        }
    }
}
