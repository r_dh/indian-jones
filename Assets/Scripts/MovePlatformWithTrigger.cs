﻿using UnityEngine;
using System.Collections;

public class MovePlatformWithTrigger : MonoBehaviour
{

    public float Speed;
    public bool Switch;
    public float Distance;
    private float minPos, maxPos;

    public static bool _isActive = false;


    // Use this for initialization
    void Start()
    {
        Speed = 7.5f;
        Switch = true;
        Distance = 20f;
        minPos = 40.0f;
        maxPos = 80f;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isActive == true)
        {
            if (transform.position.y > maxPos)
            {
                Switch = false;
            }
            else if (transform.position.y < minPos)
            {
                Switch = true;
            }
            if (Switch == true)
            {
                transform.Translate(new Vector3(0, 1, 0) * Time.deltaTime * Speed);
            }
            else
            {
                transform.Translate(new Vector3(0, 1, 0) * Time.deltaTime * -Speed);
            }
        }
    }
}