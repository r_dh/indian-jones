﻿using UnityEngine;
using System.Collections;

public class Ladder : MonoBehaviour {

    void OnTriggerStay(Collider other) {
        if (other.tag == "Player") {
            other.gameObject.GetComponent<Player_Control>().SetLadder(true);
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag == "Player") {
            other.gameObject.GetComponent<Player_Control>().SetLadder(false);
        }
    }
}
