﻿using UnityEngine;
using System.Collections;

public class LoadLevel1Script : MonoBehaviour {
	public Material mat1, mat2;
	public GameObject bords, text;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown(){
		// this object was clicked - do something
		//Destroy (this.gameObject);
		Debug.Log ("Clicked Play");
		Application.LoadLevel ("Level1");
	}

	void OnMouseOver(){
		Debug.Log ("mouse is hovering");
		text.GetComponent<MeshRenderer> ().material = mat1;
		//renderer.mat
	}

	void OnMouseExit(){
		Debug.Log ("mouse is not hovering :D");
		text.GetComponent<MeshRenderer> ().material = mat2;
	}
}
