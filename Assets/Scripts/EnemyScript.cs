﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyScript : MonoBehaviour {
    private NavMeshAgent _agent;
    private GameObject _target;
    // Use this for initialization
    void Start() 
    {
        _agent = GetComponent<NavMeshAgent>();
        _target = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update() 
    {
        if (Vector3.Distance(this.transform.position, _target.transform.position) < 15.0f && Menu3D._menuIsActive == false && _agent.enabled == true) 
        {
            _agent.destination = _target.transform.position;
            GetComponentInChildren<Animator>().SetBool("IsActive", true);
        }
        else
        {
            GetComponentInChildren<Animator>().SetBool("IsActive", false);
        }	
    }
    void OnCollisionEnter(Collision coll) 
    {
        if (coll.collider.tag == "Player") 
        {
            coll.collider.transform.position = RespawnScript.RespawnPos;
        }
    }
}
