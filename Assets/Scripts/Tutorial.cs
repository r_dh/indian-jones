﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class Tutorial : MonoBehaviour {
    public int Progress;
    public GameObject Player;
    private LineRenderer line;
    private GUIText _TextOverlay;
    private Outline _outline;
    private bool _usingController = false;

    private void Start() {
        line = gameObject.AddComponent<LineRenderer>();
        line.SetWidth(0.2f, 0.2f);
        line.SetVertexCount(2);
        line.material.color = Color.white;
        line.material.shader = Shader.Find("GUI/Text Shader");
        line.enabled = true;

        Player = GameObject.Find("Player");
       
        _TextOverlay = GameObject.Find("Tutorial").GetComponent<GUIText>();
        _TextOverlay.transform.position = new Vector3(0.1f, 0.8f, 0.0f);

        _outline = GameObject.Find("Tutorial").AddComponent<Outline>();
        _outline.effectDistance = new Vector2(1, -1);
        _outline.effectColor = Color.black;
        _outline.useGraphicAlpha = true;
        _outline.enabled = true;
    }

    private void Update() {

        if(!_usingController) if (new Vector3(Input.GetAxis("Horizontal (Controller)"), 0, 0) != Vector3.zero) _usingController = true;

        Vector3 screenpos = new Vector3(GameObject.Find("Main Camera").transform.position.x, GameObject.Find("Main Camera").transform.position.y, 0.0f);
        screenpos += new Vector3(-8f, 3f, 0.0f);

        line.SetVertexCount(2);
        line.SetPosition(0, screenpos);
        line.SetPosition(1, Player.transform.position + new Vector3(-0.75f, 1.25f, 0.0f));

        switch (Progress) {
            case 0:
                _TextOverlay.text = "Use A/Q and D\nor left joystick\nto walk";
                if (Player.transform.position.x >= 5.0f) ++Progress;
                break;
            case 1:
                _TextOverlay.text = "Boxes are moveable";               
                if (Player.transform.position.x >= 10.0f) ++Progress;
                break;
            case 2:
                _TextOverlay.text = "Press spacebar\nto jump";
                if (_usingController) _TextOverlay.text = "Press A\nto jump";
                if (Player.transform.position.x >= 24.0f) ++Progress;
                break;
            case 3:
                //Adjust position for large text
                _TextOverlay.transform.position = new Vector3(0.05f, 0.95f, 0.0f);
                _TextOverlay.text = "LMB to shoot your grapple\nRMB to let go\nPress W/Z and S to change the length of the rope\nUse movement to swing";
                if (_usingController) _TextOverlay.text = "Right Bumper to shoot your grapple\nLeft Bumper to let go\nUse movement to change\nthe rope length and to swing";
                if (Player.transform.position.x >= 100.0f) ++Progress;
                break;
            case 4:
                //Reset position
                _TextOverlay.transform.position = new Vector3(0.1f, 0.8f, 0.0f);
                _TextOverlay.text = string.Empty;
                line.SetVertexCount(0);
                if (Player.transform.position.x >= 171.0f || Player.transform.position.y <= 0.0f) ++Progress;
                break;
            case 5:
                _TextOverlay.text = "Your grapple is lethal!\nLMB to fire!";
                 if (_usingController) _TextOverlay.text = "Your grapple is lethal!\nRight Bumper to fire!";
                if (Player.transform.position.x >= 181.0f) ++Progress;
                break;
            case 6:
                _TextOverlay.text = "Move up to go up ladders";
                if (Player.transform.position.x >= 290.0f || Player.transform.position.y >= 11.0f) ++Progress;
                break;
            case 7:
                _TextOverlay.text = "Good job!\nThe end is just up ahead";
                if (Player.transform.position.x >= 205.0f) ++Progress;
                break;
            case 8:
                _TextOverlay.text = string.Empty;
                line.SetVertexCount(0);
                break;
            default:
                _TextOverlay.text = string.Empty;
                line.SetVertexCount(0);
                break;
        }

    }
}
