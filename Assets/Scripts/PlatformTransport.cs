﻿using UnityEngine;
using System.Collections;

public class PlatformTransport : MonoBehaviour {

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            other.gameObject.transform.SetParent(gameObject.transform);
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag == "Player") {
            other.gameObject.transform.parent = null;
        }
    }
}
