﻿using UnityEngine;
using System.Collections;

public class Level2PlatformTrigger : MonoBehaviour 
{
    public Material ActivatedMaterial = null;
	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnCollisionEnter(Collision coll)
    {
        if(coll.collider.tag == "Moveable")
        {
            MovePlatformWithTrigger._isActive = true;
            GetComponent<MeshRenderer>().material = ActivatedMaterial;
        }
    }
}
